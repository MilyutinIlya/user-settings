package test.group.usersettings.da.model;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "USERS")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;
    @Column(name = "login", unique = true)
    private String login;
    @OneToMany(mappedBy = "user", fetch = LAZY)
    private List<Setting> settingList;

    public User(String login) {
        this.login = login;
    }

    public User(Long id) {
        this.id = id;
    }

    public User(String login, List<Setting> settingList) {
        this.login = login;
        this.settingList = settingList;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Setting> getSettingList() {
        return settingList;
    }

    public void setSettingList(List<Setting> settingList) {
        this.settingList = settingList;
    }
}
