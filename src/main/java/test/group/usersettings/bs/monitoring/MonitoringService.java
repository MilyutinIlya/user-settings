package test.group.usersettings.bs.monitoring;

/**
 * Сервис для работы с системой мониторинга
 */
public interface MonitoringService {
    /**
     * Уведомляет систему мониторнига об ошибке
     */
     void notifyError();
}
