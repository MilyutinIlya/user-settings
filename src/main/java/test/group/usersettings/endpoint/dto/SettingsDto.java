package test.group.usersettings.endpoint.dto;

/**
 * Объект "Настройки" для передачи данных на PL
 */
public class SettingsDto {
    /**
     * id настройки
     */
    private Long id;
    /**
     * Название настройки
     */
    private String name;
    /**
     * Значение настройки
     */
    private String value;
    /**
     * Id пользователя, кому принадлежит настройка
     */
    private Long userId;

    public SettingsDto() {
    }

    public SettingsDto(String name, String value, Long userId) {
        this.name = name;
        this.value = value;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
