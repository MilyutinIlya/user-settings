package test.group.usersettings.endpoint.dto;

import java.util.List;


public class UserDto {
    private Long id;
    private String login;
    private List<SettingsDto> settings;

    public UserDto() {
    }

    public UserDto(String login) {
        this.login = login;
    }

    public UserDto(String login, List<SettingsDto> settingsIdList) {
        this.login = login;
        this.settings = settingsIdList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<SettingsDto> getSettings() {
        return settings;
    }

    public void setSettings(List<SettingsDto> settings) {
        this.settings = settings;
    }
}
