package test.group.usersettings;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import test.group.usersettings.da.model.Setting;
import test.group.usersettings.endpoint.dto.SettingsDto;

import java.util.List;


@Mapper(componentModel = "spring")
public interface SettingsMapper {
    @Mapping(source = "user.id", target = "userId")
    SettingsDto settingsToSettingsDto(Setting setting);

    List<SettingsDto> settingsToSettingsDto(List<Setting> setting);

    @Mapping(source = "userId", target = "user.id")
    Setting settingsDtoToSettings(SettingsDto settingsDto);

}
