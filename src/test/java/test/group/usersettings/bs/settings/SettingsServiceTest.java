package test.group.usersettings.bs.settings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import test.group.usersettings.SettingsMapper;
import test.group.usersettings.SettingsMapperImpl;
import test.group.usersettings.da.model.Setting;
import test.group.usersettings.da.model.User;
import test.group.usersettings.da.repository.SettingsRepository;
import test.group.usersettings.endpoint.dto.SettingsDto;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class SettingsServiceTest {
    @InjectMocks
    private SettingsService settingsService;
    @Mock
    private SettingsRepository settingsRepository;
    @Spy
    private SettingsMapper settingsMapper = new SettingsMapperImpl();

    @Test
    void createSettings() {
        Mockito.when(settingsRepository.findBySettingNameAndUserId(any(), any())).thenReturn(null);
        Setting setting = new Setting(1L, "name", "value", new User());
        setting.setId(1L);
        Mockito.when(settingsRepository.save(any())).thenReturn(setting);

        SettingsDto settingsDto = settingsService.createSettings(new SettingsDto("name", "value", 1L));

        Assertions.assertEquals(settingsDto.getId(), setting.getId());

    }

}